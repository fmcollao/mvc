<?php

class Controller{
	protected $modelo;
	protected $vista;

	public function modelo($modelo)
	{
		$this->modelo = $modelo;
		require_once __DIR__ . '/../models/'. $this->modelo .'.php';

		return new $this->modelo;
	}

	public function render($vista, $datos = [])
	{
		$this->vista = $vista;
		require_once __DIR__ . '/../views/'.$this->vista.'.phtml';
	}
}