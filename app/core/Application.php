<?php

class Application{
	protected $controlador = null;
	protected $metodo      = null;
	protected $params      = [];

	public function __construct()
	{
		$url = $this->parseUrl();

		if (file_exists(__DIR__ . '/../controllers/'. ucwords($url[0]) .'Controller.php')) {
			$this->controlador = ucwords($url[0]) . 'Controller';
			unset($url[0]);
		}else {
			$this->controlador = 'HomeController';
		}

		require_once __DIR__ . '/../controllers/'. $this->controlador .'.php';
		$this->controlador = new $this->controlador;

		if (isset($url[1])) {
			if (method_exists($this->controlador, $url[1])) {
				$this->metodo = $url[1];
				unset($url[1]);
			}
		}else {
			$this->metodo = 'index';
		}

		$this->params = $url ? array_values($url) : [];

		call_user_func_array([$this->controlador, $this->metodo], $this->params);
	}

	private function parseUrl()
	{
		if (isset($_GET['url'])) {
			return $url = explode('/', filter_var(rtrim($_GET['url'], ''), FILTER_SANITIZE_URL));
		}
	}
}